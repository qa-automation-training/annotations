package fullAnnotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
 

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@interface MyAnnotation{
    int studentAge() default 18;
    String studentName();
    String stuAddress();
    String stuStream() default "CSE";
}
public class FullAnnotation {
    
    @MyAnnotation(
        studentName="Chaitanya",
        stuAddress="Agra, India"
    )
    public void test() {
    
    }     
    
}
