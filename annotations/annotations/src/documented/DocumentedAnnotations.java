package documented;

import java.lang.annotation.*;  
import java.lang.reflect.*; 
import java.lang.annotation.Documented;

@Documented
@interface Crunchify_Documented {
   String writeDocument();
}
public class DocumentedAnnotations {

	public static void main(String arg[]) {
	      new DocumentedAnnotations().performDocumented();
	   }
	 
	   @Crunchify_Documented(writeDocument="Hello document")
	   public void performDocumented() {
	      System.out.printf("Testing annotation 'Crunchify_Documented'");
	   }
}
