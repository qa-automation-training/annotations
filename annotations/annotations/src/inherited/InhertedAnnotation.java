package inherited;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;



public class InhertedAnnotation {
    
    public static void main(String[] args) throws Exception {
        System.out.println(Bar.class.isAnnotationPresent(InheritedAnnotation.class));
        
    }
}

@Retention(RetentionPolicy.RUNTIME)
@Inherited
@interface InheritedAnnotation {
}

@InheritedAnnotation
class Foo {

}

class Bar extends Foo {
}
   



