package retention;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
//@Retention(RetentionPolicy.CLASS)
//@Retention(RetentionPolicy.SOURCE)
@Retention(RetentionPolicy.RUNTIME)
@interface MySampleAnn {

    String name();
    String desc();
}

public class RetentionAnnotation {
    @MySampleAnn(name = "test1", desc = "testing annotations")
    public void myTestMethod(){
        //method implementation
    }

}
