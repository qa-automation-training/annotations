package safeVarArgs;


import java.util.Arrays;
import java.util.List;

public class SafeVarArgs {
    @SafeVarargs
    static void displayList(List<String>... lists) {
        for (List<String> list : lists) {
          System.out.println("From displayList : \n"+list);
        }
      }
    @SafeVarargs
    private final void displayList1(List<String>... lists) {
        for (List<String> list : lists) {
          System.out.println("From displayList1 : \n"+list);
        }
      }

      public static void main(String args[]) {
          List<String> programmingLanguages = Arrays.asList("Java", "C","python");
          SafeVarArgs.displayList(programmingLanguages);
          SafeVarArgs obj = new SafeVarArgs();
          obj.displayList1(programmingLanguages);
      }
}
