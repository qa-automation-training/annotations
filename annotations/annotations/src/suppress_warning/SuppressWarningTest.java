package suppress_warning;

import java.util.ArrayList;
import java.util.List;

public class SuppressWarningTest {

        // If we comment below annotation, program generates 
        // warning 
        @SuppressWarnings({"unchecked", "deprecation"}) 
        public static void main(String args[]) 
        { 
            //for deprecation 
        	suppress_warning d1 = new suppress_warning(); 
            d1.Display(); 
           // for unchecked warnings 
            List<String> words = new ArrayList<String>();
            words.add("hello");
           
        } 
        
     
}
